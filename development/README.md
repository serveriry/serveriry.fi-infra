# Serveri ry development infra

This is development environment for serveri ry infrastructure.

## Running environment

See [dependencies](/README.md#dependencies) for environment.

Run command ``vagrant up`` in the path that contain [Vagrantfile](/development/Vagrantfile)

View the webpage in [localhost:8080](http://127.0.0.ssh1:8080/).

---

## Vagrant Commands

### Start vagrant / virtual machine

``vagrant up``

### Run VM configuration files again (ansible playbooks)

``vagrant provision``

### Interact with virtual machine

``vagrant ssh``

### Stop virtual machine

``vagrant halt``

### Delete vagrant / virtual machine

``vagrant destroy``

### Restart vagrant

``vagrant reload``

- works for reloading vagrantfile?? at least port forward stuff.

### Pause / unpause vagrant

- ``vagrant suspend``
- ``vagrant resume``

---

## Directus DB import / export

Run these commands inside vm (`vagrant ssh`)

### Export database

````shell
# Dump psql db
docker exec postgresql bash -c "pg_dump -U serveri -d serveri_prod > /home/backup.sql"

# Copy psql dump to VM 
docker cp postgresql:/home/backup.sql /home/www/backup.sql
````

### Import database

Before importing DB, make sure docker volume `www_directus_data_prod` does not contain any directus data.

This can be done by the following:

````shell

# shut down all docker containers
docker compose -f docker-compose.prod.yml down

# remove docker volume
docker volume rm www_directus_data_prod

# start psql container
docker compose -f docker-compose.prod.yml up -d db
````

Then import SQL dump:

````shell
# Copy dump from nuxt project to psql docker
docker cp /home/www/backup.sql postgresql:/home/backup.sql

# Run dump file in psql 
docker exec postgresql bash -c "psql -U serveri serveri_prod < /home/backup.sql"

# DROP existing DB - Does not work (cannot drop the currently open database)
docker exec postgresql bash -c 'psql -U serveri -d serveri_prod -c "DROP DATABASE serveri_prod WITH (FORCE);"'
````

Lastly start all docker services:

````shell
# Start rest of docker services
docker compose -f docker-compose.prod.yml up -d --build
````
